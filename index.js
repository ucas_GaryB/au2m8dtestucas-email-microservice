console.log('Loading function');

const AWS = require('aws-sdk');
const doc = require('dynamodb-doc');

const dynamo = new doc.DynamoDB();
const documentClient = new AWS.DynamoDB.DocumentClient();

var defaultTableName = process.env.TableName;

function bundleResponse(resCode, resBody) {
    // The output from a Lambda proxy integration must be 
    // in the following JSON object. The 'headers' property 
    // is for custom response headers in addition to standard 
    // ones. The 'body' property  must be a JSON string. For 
    // base64-encoded payload, you must also set the 'isBase64Encoded'
    // property to 'true'.
    let response = {
        statusCode: resCode,
        headers: {
            'Content-Type': 'application/json',
            'x-custom-header': 'au2m8dtestucas-email-microservice'
        },
        body: resBody
    };
    console.log("response: " + JSON.stringify(response))
    return response;
}


exports.handler = (event, context, callback) => {
    console.log('Received event:', JSON.stringify(event, null, 2));
    var date = (new Date()).toString().split(' ').splice(1, 4).join('-').replace(/:/g, '-');
    var foundItemsArray = [];
    var deleteBatchArray = [];
    var params = {
        TableName: defaultTableName,
        ProjectionExpression: "SESMessageId, SnsPublishTime, SESMailTimestamp, SESSourceAddress, SESDestinationAddress, SESSubject, Payload",
        ExpressionAttributeValues: {},
        /*KeyFilterExpression: keyFilterExpression,*/
        /*IndexName: "SESDestinationAddress-SESSourceAddress-index"*/
    };
    var bacthparams = {
        RequestItems: {},
    };
    var responseBody = {
        ScannedCount : 0,
        Items: [],
        Count: 0
    }
    const done = (err, res) => {
        if (event.httpMethod == 'DELETE' && res)
        {
            res.deletedItems = foundItemsArray;
            res.Count = foundItemsArray.length;
        }
        callback(null,
            bundleResponse(err ? '400' : '200', err ? err.message : JSON.stringify(res))
        )
    };

    const scanUntilDone = (err, scanResults) => {
        console.log("params:",params);
        if (err) 
        {
            console.log(err, err.stack); // an error occurred
            callback(null,
                bundleResponse('400', err.message));
        }
        else     
        {
            //console.log("scanResults:",scanResults);           // successful response                                
            
            //Record the cumalative results
            responseBody.Count = responseBody.Count + scanResults.Count;
            responseBody.ScannedCount = responseBody.ScannedCount + scanResults.ScannedCount;
            console.log("current responseBody.ScannedCount:", responseBody.ScannedCount);
            console.log("current responseBody.Count:", responseBody.Count);

            if (scanResults.Count > 0)
            {    
                scanResults.Items.forEach(element => {
                    var deleteObject = {
                        DeleteRequest: {
                            Key: {
                            "SESMessageId": element.SESMessageId, 
                            "SnsPublishTime": element.SnsPublishTime 
                            }
                        }
                        };
                    deleteBatchArray.push(deleteObject);
                    foundItemsArray.push(element);
                    console.log("DeleteRequest:", deleteObject);
                });
            }

            if (scanResults.LastEvaluatedKey) {
                params.ExclusiveStartKey = scanResults.LastEvaluatedKey;
          
                dynamo.scan(params, scanUntilDone);
            } 
            else 
            {
                responseBody.Items = foundItemsArray;             
                if (event.httpMethod == 'DELETE')
                {
                    // all scanResultss processed. done
                    var temp = { };
                    temp[tableName] = deleteBatchArray;

                    bacthparams.RequestItems = temp;
                    responseBody['bacthparams'] = temp;
                    
                    //console.log("DELETE responseBody:", responseBody);                    
                    console.log("DELETE bacthparams.RequestItems:", bacthparams.RequestItems); 

                    //Use for debugging instead of batchwrite
                    //done(null, responseBody);
                    if (responseBody.Count > 0)
                    {
                        console.log("About to batch delete:");
                        dynamo.batchWriteItem(bacthparams, done);
                    }
                    else
                    {
                        done(new Error(`No items found matching query parameters`));
                    }
                }
                else
                {   //GET                  
                    done(null, responseBody);
                }
                
            }
            
        }
    };

    if (event.queryStringParameters == null) {
        callback(null,
            bundleResponse('400', "Unsupported query. Please add query parameters"));
    }
    else {
        var tableName = event.queryStringParameters.TableName || defaultTableName;
       
        var source = (event.queryStringParameters.source || "").toLowerCase();
        var destination = (event.queryStringParameters.destination || "").toLowerCase();
        var emailafter = (event.queryStringParameters.emailafter || "");
        var emailbefore = (event.queryStringParameters.emailbefore || "");
        var subject = (event.queryStringParameters.subject || "");

        //This is an exclusive query parameter, no other values should be set if this is used.
        var messageId = (event.queryStringParameters.messageId || "");

        if (messageId.length > 0 && (source.length > 0 || destination.length > 0 || emailafter.length > 0 || emailbefore.length > 0 || subject.length > 0))        
        {
            done(new Error(`Unsupported query. If messageId is used, no other query parameters can be used`));
        }
        else if (messageId.length == 0 && source.length == 0 && destination.length == 0 && emailafter.length == 0 && emailbefore.length == 0 && subject.length == 0)
        {
            done(new Error(`Unsupported query. No valid query parameters have been used`));
        }
        else
        {     
                
            var keyFilterExpression = null;
            var filterExpression = null;
            var filterExpressionSESDestinationAddress =  "contains (SESDestinationAddress, :v1)";
            var filterExpressionSESSourceAddress =  "contains (SESSourceAddress, :v2)";
            var filterExpressionSESSubject =  "contains (SESSubject, :s1)";
            var keyFilterExpressionSESDestinationAddress = "SESDestinationAddress = :v1";
            var keyFilterExpressionSESSourceAddress = "SESSourceAddress = :v2";
            var keyFilterExpressionSESMessageId = "SESMessageId = :m1";
            var scanFilterExpression = null;
            params.TableName = tableName;

            switch (event.httpMethod) {           
                case 'GET':
                    
                    
                    //Only messageId
                    if (messageId.length > 0 
                        && source.length == 0 && destination.length == 0 && emailafter.length == 0 && emailbefore.length == 0 && subject.length == 0)
                    {
                        console.log("messageId : " + messageId);
                        params.ExpressionAttributeValues =  {};
                        params.ExpressionAttributeValues[":m1"] = messageId;  

                        params.ExpressionAttributeValues[":d3"] = "1970-01-01T00:00:00";   
                        params.ExpressionAttributeValues[":d4"] = "2050-01-01T00:00:00"; ;   

                        var dateFilterExpression = "SESMailTimestamp BETWEEN :d3 and :d4";
                        keyFilterExpression = keyFilterExpressionSESMessageId;
                        scanFilterExpression = dateFilterExpression + " AND " + keyFilterExpressionSESMessageId;
                    }

                    //Only source
                    if (source.length > 0 && destination.length == 0)
                    {
                        console.log("source : " + source);
                        params.ExpressionAttributeValues =  {};
                        params.ExpressionAttributeValues[":v2"] = source;  

                        keyFilterExpression = keyFilterExpressionSESSourceAddress;
                        scanFilterExpression = filterExpressionSESSourceAddress;
                        params.IndexName = "SESSourceAddress-index";
                    }
                    //Only destination
                    if (source.length == 0 && destination.length > 0)
                    {
                        console.log("destination : " + destination);
                        params.ExpressionAttributeValues =  {};
                        params.ExpressionAttributeValues[":v1"] = destination;   

                        keyFilterExpression = keyFilterExpressionSESDestinationAddress;
                        scanFilterExpression = filterExpressionSESDestinationAddress;
                        params.IndexName = "SESDestinationAddress-index";
                    }
                    //source and destination
                    if (source.length > 0 && destination.length > 0)
                    {
                        console.log("destination : " + destination + " and source : " + source);
                        params.ExpressionAttributeValues =  {};
                        params.ExpressionAttributeValues[":v1"] = destination; 
                        params.ExpressionAttributeValues[":v2"] = source; 
                        
                        keyFilterExpression = keyFilterExpressionSESSourceAddress + " AND " + keyFilterExpressionSESDestinationAddress;
                        scanFilterExpression = filterExpressionSESSourceAddress + " AND " + filterExpressionSESDestinationAddress;
                        params.IndexName = "SESDestinationAddress-SESSourceAddress-index";
                    }
                    
                    if (emailbefore.length > 0 || emailafter.length > 0)
                    {   //Can only be applied with other query parameter 
                        if (source.length > 0 || destination.length > 0)  
                        {
                            console.log("emailafter : " + emailafter);
                            console.log("emailbefore : " + emailbefore);
                            params.ExpressionAttributeValues[":d3"] = (emailafter.length > 0) ? emailafter : "1970-01-01T00:00:00";   
                            params.ExpressionAttributeValues[":d4"] = (emailbefore.length > 0) ? emailbefore : "2050-01-01T00:00:00"; ;   

                            var dateFilterExpression = "SESMailTimestamp BETWEEN :d3 and :d4";
                            filterExpression = dateFilterExpression; 
                            scanFilterExpression = dateFilterExpression + " AND " + scanFilterExpression;
                        }
                        else
                        {
                            done(new Error(`Unsupported query. Please add either or both source and destination query parameters`));
                        }
                    }

                    if (subject.length > 0 )
                    {                    
                        params.ExpressionAttributeValues[":s1"] = subject;  
                        scanFilterExpression = (scanFilterExpression != null)?scanFilterExpression + " AND " + filterExpressionSESSubject:filterExpressionSESSubject;
                    }
                    
                    //Only used if we are using if query is used
                    //params.KeyFilterExpression = keyFilterExpression;
                    if (scanFilterExpression != null)
                        params.FilterExpression = scanFilterExpression;
                    
                    console.log("params", params);
                    
                    //documentClient.query(params, done);
                    //documentClient.scan(params, done);
                    documentClient.scan(params, scanUntilDone);

                    break;

                case 'DELETE':

                    console.log("DELETE destination : " + destination + " , source : " + source + " , messageId : " + messageId);
                    if ((source.length > 0 && destination.length > 0) || messageId.length > 0)
                    {                     

                        if (messageId.length > 0 //Only messageId
                            && source.length == 0 && destination.length == 0 && emailafter.length == 0 && emailbefore.length == 0 && subject.length == 0)
                        {
                            console.log("messageId : " + messageId);
                            params.ExpressionAttributeValues =  {};
                            params.ExpressionAttributeValues[":m1"] = messageId;  

                            params.ExpressionAttributeValues[":d3"] = "1970-01-01T00:00:00";   
                            params.ExpressionAttributeValues[":d4"] = "2050-01-01T00:00:00"; ;   

                            var dateFilterExpression = "SESMailTimestamp BETWEEN :d3 and :d4";
                            keyFilterExpression = keyFilterExpressionSESMessageId;
                            scanFilterExpression = dateFilterExpression + " AND " + keyFilterExpressionSESMessageId;
                        }
                        else
                        {
                            console.log("destination : " + destination + " and source : " + source);
                            params.ExpressionAttributeValues =  {};
                            params.ExpressionAttributeValues[":v1"] = destination; 
                            params.ExpressionAttributeValues[":v2"] = source; 
                            
                            keyFilterExpression = keyFilterExpressionSESSourceAddress + " AND " + keyFilterExpressionSESDestinationAddress;
                            scanFilterExpression = filterExpressionSESSourceAddress + " AND " + filterExpressionSESDestinationAddress;
                            params.IndexName = "SESDestinationAddress-SESSourceAddress-index";

                            if (emailbefore.length > 0 || emailafter.length > 0)
                            {   
                                console.log("emailafter : " + emailafter);
                                console.log("emailbefore : " + emailbefore);
                                params.ExpressionAttributeValues[":d3"] = (emailafter.length > 0) ? emailafter : "1970-01-01T00:00:00";   
                                params.ExpressionAttributeValues[":d4"] = (emailbefore.length > 0) ? emailbefore : "2050-01-01T00:00:00"; ;   

                                var dateFilterExpression = "SESMailTimestamp BETWEEN :d3 and :d4";
                                filterExpression = dateFilterExpression; 
                                scanFilterExpression = dateFilterExpression + " AND " + scanFilterExpression;                      
                            }

                            if (subject.length > 0 )
                            {                    
                                params.ExpressionAttributeValues[":s1"] = subject;  
                                scanFilterExpression = (scanFilterExpression != null)?scanFilterExpression + " AND " + filterExpressionSESSubject:filterExpressionSESSubject;
                            }
                        }

                        //Only used if we are using if query is used
                        //params.KeyFilterExpression = keyFilterExpression;
                        if (scanFilterExpression != null)
                            params.FilterExpression = scanFilterExpression;
                        
                        console.log("params:",params);                        

                        //documentClient.query(params, done);
                        documentClient.scan(params,scanUntilDone);

                    }
                    else
                    {
                        done(new Error(`Unsupported query. Please add both source and destination query parameters`));
                    }
                    break;    
                default:
                    done(new Error(`Unsupported method "${event.httpMethod}"`));
            }
        }
    }
};
